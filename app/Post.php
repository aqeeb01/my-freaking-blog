<?php

namespace App;

use Carbon\Carbon;

class Post extends Model
{
    //protected $fillable = ['title', 'body'];
	public function comments()
	{
		return $this->hasMany(Comment::class);
	}
	public function addComment($body)
	{
		// Comment::create([
  //   		'body' => request('body'),
  //   		'post_id' => $this->id
  //   	]);

		//another way to add comments
		$this->comments()->create(compact('body'));
	}
	public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function scopeFilter($query, $filters)
    {
    	if (isset($filters['month']))
        {
            $query->whereMonth('created_at', Carbon::parse($filters['month'])->month);
        }
            
        if (isset($filters['year'])) 
        {
            $query->whereYear('created_at', $filters['year']);
        }
            
        if (isset($filters['recent'])) 
        {
            $query->where( 'created_at', '>', Carbon::now()->subDays(14));
        }        

    	// if ($month = $filters['month'])
    	// {
    	// 	$query->whereMonth('created_at', Carbon::parse($month)->month);
    	// }
    	// if ($year = $filters['year'])
    	// {
    	// 	$query->whereYear('created_at', $year);
    	// }
    }
    public static function archives()
    {
    	return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
        ->groupBy('year', 'month')
        ->orderByRaw('min(created_at) desc')
        ->get()
        ->toArray();
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
