<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }

    public function create()
    {
    	return view('sessions.create');
    }

    public function store()
    {
         //attempt to signin
        if(! auth()->attempt(request(['email', 'password'])))
        {
            return back()->withErrors([
                'message' => 'Please Check the login info']);
        }
        //if not, redirect back
        //if so, sign in 

        session()->flash('message', 'YAY! Signed in!');
        //redirect to home page
        return redirect()->home();
    }

    public function destroy()
    {
    	auth()->logout();
    	
        session()->flash('message', 'Thank you for getting out! Hope you had not created a mess of everything as usual');
    	return redirect()->home();
    }
}
