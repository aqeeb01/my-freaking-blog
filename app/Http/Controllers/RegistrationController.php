<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationForm;

class RegistrationController extends Controller
{
    //
    public function create()
    {
    	return view('registration.create');
    }
    public function store(RegistrationForm $form)
    {
        //validate the info
    	// $this->validate(request(), [
    	// 	'name' => 'required',
    	// 	'email' => 'required|email',
    	// 	'password' => 'required|confirmed'
    	// ]);

    	//create and save user
    	//sign in
        //send a welcome email
        $form->persist();

        session()->flash('message', 'Thank you for signing up on my USELESS website, I guess?');
    	//redirect to home page
    	return redirect()->home();
    }
}
