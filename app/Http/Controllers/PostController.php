<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Repositories\Posts;
use Carbon\Carbon;

class PostController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Posts $posts)
    {
    	//$posts = Post::latest()->get();
        $posts = Post::latest()->filter(request(['month', 'year']))->get();
        //$posts = $posts->all();
        //$posts = (new \App\Repositories\Posts)->all();

        //$archives = Post::archives();
        //$posts = Post::orderBy('title', 'asc')->paginate(2);
    	return view ('posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
    	return view ('posts.show', compact('post'));
    }

    public function create()
    {
    	return view ('posts.create');
    }

    public function store()
    {
    	//$post = new \App\Post;
		//$post = new Post;

		// $post->title = request('title');
		// $post->body = request('body');
		// //save to the DB
		// $post->save();

		//another way: 
		// Post::create([
		// 	'title' = request('title'),
		// 	'body' = request('body')
		// ]);

		//another way:
		
		$this->validate(request(), [
				'title' => 'required|min:10',
				'body' => 'required'
			]);
        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );
        //Post::create(request(['title', 'body', 'user_id']));

		return redirect('/');
    }
}
