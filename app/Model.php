<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

class Model extends Eloquent
{
    //protected $fillable = ['title', 'body'];
    protected $guarded = [];
}
