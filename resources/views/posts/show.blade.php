@extends('layouts.layout')

@section('content')
	<h2 class="blog-post-title">
    	{{ $post->title }}
    </h2>

    @if (count($post->tags))
    	<ul>
    		@foreach ($post->tags as $tag)
    			<li>
    				<a href="/posts/tags/{{ $tag->name }}">
    					{{ $tag->name }}
    				</a>
    			</li>
    		@endforeach
    	</ul>
    @endif

     {{ $post->body }}

    <hr>

    @foreach ($post->comments as $comment)
    	<li class="list-group-item">
    		<strong>
    			{{ $comment->created_at->diffForHumans() }} :&nbsp;
    		</strong>

    		{{ $comment->body }}
    	</li>
    @endforeach

	<hr>

	<div class="card">
		<div class="card-block">
			<form method="post" action="/posts/{{ $post->id }}/comments">
				@csrf
				<div class="form-group">
					<textarea name="body" placeholder="Place your comments here..." class="form-control" required=""></textarea>
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Add Comment</button>
				</div>
			</form>
			@include('layouts.errors')
		</div>
	</div>
@endsection