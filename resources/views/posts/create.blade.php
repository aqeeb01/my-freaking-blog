@extends('layouts.layout')
@section('content')
		<h3 class="pb-3 mb-4 font-italic border-bottom">
            Create a blog post
        </h3>
          
        <form method="post" action="/posts" class="form-signin">
            @csrf
            <label for="title">Blog Title: </label>
            <input type="text" id="title" class="form-control" name="title" required autofocus>

            <label for="body">Body</label>
            <textarea id="body" name="body" class="form-control" required></textarea>
            <br>
          
          <button class="btn btn-primary" type="submit">Publish</button>
          <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
          <br>
          @include ('layouts.errors')
          
        </form>
@endsection