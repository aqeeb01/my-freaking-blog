@extends('layouts.layout')

@section('content')

	<h1> Login </h1>

	<form method="post" action="/login">
		@csrf

		<div class="form-group">
			<label for="email">Email:</label>
			<input type="email" name="email" id="email" class="form-control" required>
		</div>

		<div class="form-group">
			<label for="password">Password:</label>
			<input type="password" name="password" id="password" class="form-control">
		</div>

		<div class="form-group">
			<button class="btn btn-primary" type="submit">Sign in</button>
		</div>

		<div class="form-group">
			@include ('layouts.errors')
		</div>
	</form>
@endsection